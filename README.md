# MobTimer

MobTimer is a small ReactJS application to timebox mob programming sessions.

## Status

MVP. Minimal features but is usable and does the job.

It was put together very quickly to scratch an itch. Code may be messy etc.

Works with Chrome and Firefox.

### Development

MobTimer uses webpack. After checking out the project run

```
$ npm install
$ npm start
```

This will start the webpack-dev-server on port 8080.

### TODOs

- UI/UX concept
- decouple UI
- make mobster list elements sortable (drag'n'drop)
- customizable gong sounds (maybe on per-user basis)
- ability to pause the timer
- disable mobsters without deleting
- support all modern browsers

## License

Copyright © 2015 Stefan Oestreicher and contributors.

Distributed under the terms of the BSD-3-Clause license.
