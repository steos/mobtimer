'use strict';

import Immutable from 'immutable';
import React from 'react';
import ReactDOM from 'react-dom';

import App from './components/App';

function loadAudio(path) {
    let audio = new Audio(path);
    return new Promise(function(resolve) {
        audio.addEventListener('canplaythrough', function() {
            resolve(audio);
        });
    });
}

function render(sound) {
    ReactDOM.render(<App sound={sound} />, window.mountNode);
}

function init(gong) {
    Notification.requestPermission(function (perm) {
        //handle denied
    });
    setInterval(render.bind(null, gong), 200);
    render(gong);
}

loadAudio('/gong1.wav').then(gong => init(gong));
