"use strict";

import _ from 'lodash';
import Immutable from 'immutable';
import React from 'react';
import * as bs from 'react-bootstrap';

import MobsterList from './MobsterList';
import MobsterForm from './MobsterForm';
import Countdown from './Countdown';

export default React.createClass({
    propTypes: {
        sound: React.PropTypes.instanceOf(Audio).isRequired
    },
    getInitialState: function() {
        const stateJson = localStorage.getItem('appState');
        const state = stateJson ? JSON.parse(stateJson) : {};
        if (state.mobsters) {
            state.mobsters = Immutable.fromJS(state.mobsters); // brr
        }
        return _.assign({
            start: null,
            duration: 10, //minutes
            mobsters: Immutable.List(),
            currentMobster: 0,
            playSound: true
        }, state);
    },

    componentDidUpdate() {
        this.persistState();
    },

    persistState() {
        localStorage.setItem('appState', JSON.stringify(this.state));
    },

    removeMobster: function(index) {
        this.setState({
            mobsters: this.state.mobsters.delete(index),
            currentMobster: this.state.currentMobster === index
                ? Math.max(0, Math.min(this.state.mobsters.size - 2, index))
                : (index > this.state.currentMobster
                ? this.state.currentMobster
                : this.state.currentMobster - 1)
        });
    },
    setCurrentMobster: function(index) {
        this.setState({currentMobster: index});
    },
    saveMobster: function(mobster) {
        this.setState({
            mobsters: this.state.mobsters.push(Immutable.fromJS(mobster))
        });
    },
    getTimeDelta: function() {
        if (this.state.start === null) return null;
        let elapsed = (new Date()).getTime() - this.state.start;
        return this.state.duration * 60000 - elapsed;
    },
    dispatchNotifyInterval: function() {
        let interval = setInterval(function() {
            let delta = this.getTimeDelta();
            if (delta === null) {
                // timer was stopped prematurely
                clearInterval(interval);
                return;
            }
            if (delta <= 0) {
                this.showNotification(this.getNextMobster());
                this.playSoundMaybe();
                clearInterval(interval);
            }
        }.bind(this), 200);
    },

    showNotification(next) {
        let notification = new Notification('Switch it up!', {
            body: next.get('name') + ', it\'s your turn!'
        });
        notification.onclick = function() {
            if (this.getTimeDelta() !== null) {
                this.switchMobster();
            }
        }.bind(this);
    },

    playSoundMaybe() {
        if (!this.state.playSound) return;
        this.props.sound.pause();
        this.props.sound.currentTime = 0;
        this.props.sound.play();
    },
    startTimer: function() {
        this.setState({
            start: (new Date()).getTime()
        });
        this.dispatchNotifyInterval();
    },
    stopTimer: function() {
        this.setState({start: null});
    },
    getNextMobster: function() {
        return this.state.mobsters.get((this.state.currentMobster + 1) % this.state.mobsters.size);
    },
    switchMobster: function() {
        this.setState({
            currentMobster: (this.state.currentMobster + 1) % this.state.mobsters.size,
            start: (new Date()).getTime()
        });
        this.dispatchNotifyInterval();
    },
    changeDuration: function(e) {
        let val = parseInt(e.target.value.trim(), 10);
        if (val > 0) {
            this.setState({duration: Math.max(1, val)});
        }
    },

    renderMainView() {
        return (
            <div>
                <bs.Input addonAfter="minutes"
                          type="number"
                          min="1"
                          value={this.state.duration}
                          onChange={this.changeDuration}/>
                <MobsterForm onSave={this.saveMobster}/>
                <MobsterList onSelect={this.setCurrentMobster}
                             current={this.state.currentMobster}
                             mobsters={this.state.mobsters}
                             remove={this.removeMobster}/>
                <bs.Button bsStyle="primary"
                           disabled={this.state.mobsters.size == 0}
                           onClick={this.startTimer}>Start</bs.Button>
            </div>
        );
    },

    renderCountdownView() {
        return <Countdown delta={this.getTimeDelta()}
                          current={this.state.mobsters.get(this.state.currentMobster)}
                          next={this.getNextMobster()}
                          onSwitch={this.switchMobster}
                          onStop={this.stopTimer}/>;
    },

    render: function() {
        return (
            <div>
                <h1>MobTimer</h1>
                <bs.Glyphicon style={{'fontSize':'2em', 'cursor': 'pointer'}}
                              glyph={this.state.playSound ? "volume-up" : "volume-off"}
                              onClick={e => this.setState({playSound: !this.state.playSound})}/>
                {this.state.start === null
                    ? this.renderMainView()
                    : this.renderCountdownView()}
            </div>
        );
    }
});
