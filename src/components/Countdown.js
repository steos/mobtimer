"use strict";

import Immutable from 'immutable';
import React from 'react';
import * as bs from 'react-bootstrap';
import moment from 'moment';
import {} from 'moment-duration-format';

export default React.createClass({
    propTypes: {
        current: React.PropTypes.instanceOf(Immutable.Map).isRequired,
        next: React.PropTypes.instanceOf(Immutable.Map).isRequired,
        delta: React.PropTypes.number.isRequired,
        onSwitch: React.PropTypes.func.isRequired,
        onStop: React.PropTypes.func.isRequired
    },
    renderFinished() {
        return (
            <div>
                <h2>{this.props.next.get('name')}, it's your turn!</h2>
                <bs.Button bsStyle="primary" onClick={this.props.onSwitch}>Switch</bs.Button>
                <bs.Button bsStyle="danger" onClick={this.props.onStop}>Stop</bs.Button>
            </div>
        );
    },
    renderRunning() {
        return (
            <div>
                <h2>{moment.duration(this.props.delta).format('mm:ss', {trim:false})}</h2>
                Current Mobster: {this.props.current.get('name')}<br/>
                Up next: {this.props.next.get('name')}<br/>
                <bs.Button bsStyle="danger" onClick={this.props.onStop}>Stop</bs.Button>
            </div>
        );
    },
    render() {
        return this.props.delta <= 0 ? this.renderFinished() : this.renderRunning();
    }
});
