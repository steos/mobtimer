"use strict";

import * as bs from 'react-bootstrap';

import Immutable from 'immutable';
import React from 'react';
import MobsterForm from './MobsterForm';

export default React.createClass({
    propTypes: {
        mobsters: React.PropTypes.instanceOf(Immutable.Collection).isRequired,
        current: React.PropTypes.number.isRequired,
        onSelect: React.PropTypes.func.isRequired,
        remove: React.PropTypes.func.isRequired
    },
    renderMobsterItem: function(mobster, index) {
        return (
            <li key={index}>
                {mobster.get('name')}
                <bs.ButtonToolbar>
                    <bs.Button bsStyle="link" onClick={this.props.onSelect.bind(null, index)}>
                        <bs.Glyphicon glyph={this.props.current === index ? 'heart' : 'heart-empty'}/>
                    </bs.Button>
                    <bs.Button bsStyle="link" onClick={this.props.remove.bind(null, index)}>
                        <bs.Glyphicon glyph="trash"/>
                    </bs.Button>
                </bs.ButtonToolbar>
            </li>
        );
    },
    render() {
        return this.props.mobsters.size > 0
            ? <ul className="mobster-list">{this.props.mobsters.map(this.renderMobsterItem)}</ul>
            : <p className="lead text-muted">Add a mobster!</p>;
    }
});
