"use strict";

import React from 'react';
import * as bs from 'react-bootstrap';

export default React.createClass({
    propTypes: {
        onSave: React.PropTypes.func.isRequired
    },
    getInitialState: function() {
        return {name: ''}
    },
    isValidMobster: function() {
        return this.state.name.trim().length > 0;
    },
    changeName: function(e) {
        this.setState({
            name: e.target.value.trim()
        });
    },
    save: function() {
        this.props.onSave(this.state);
        this.setState({name: ''});
    },
    handleKey: function(e) {
        if (e.key === 'Enter' && this.isValidMobster()) {
            this.save();
        }
    },
    render: function() {
        var valid = this.isValidMobster();
        let saveBtn = <bs.Button disabled={!valid} onClick={this.save}>Save</bs.Button>;
        return (
            <div>
                <label>Add Mobster:</label><br/>
                <bs.Input type="text"
                          value={this.state.name}
                          onChange={this.changeName}
                          buttonAfter={saveBtn}
                          onKeyPress={this.handleKey}/>
            </div>
        );
    }
});
